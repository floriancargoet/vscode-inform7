# Inform 7 extension for VS Code

This extension adds support for [Inform 7](http://inform7.com/) in VS Code.

## Features

### Syntax highlighting

The extension adds syntax highlighting to Inform 7 sources (`.ni` file extension) and extensions (`.i7x` file extension).

It is recommended to also download the [Inform 6 extension](https://marketplace.visualstudio.com/items?itemName=natrium729.inform-6), so that Inform 6 inclusions in Inform 7 source are coloured.

Additionally, texts used in Vorple's `execute JavaScript command` phrase will have JavaScript highlighting. In other situations, if we want to specify that a text contains a JavaScript command, we can precede it by the comment `[js]`:

```
The JS instruction is a text that varies.
The JS instruction is [js]"window.alert('Hello!')".
```

This is useful for texts that varies or properties.

The extension also highlights Preform (`.preform` file extension), but this is likely only of interest to those writing language extensions for Inform 7. Since the syntax for including Inform 6 and Preform in Inform 7 is the same (`(-` and `-)`), we should, similarly than for JS commands, add the comment `[preform]` right before the opening parenthesis, so that Preform is highlighted correctly.

Not doing this may cause the colouring of the inclusion to leak outside of the parentheses.

### Compiling a story from VS Code

The extension adds a button in the toolbar, at the top right of the window, when an Inform 7 file is open. Clicking it will compile the story. If there are errors during the compilation, they are shown in the Problems panel. The `openStoryAfterCompilation` setting can make the newly compiled story open automatically.

Clicking the button while pressing `Alt` (or `Shift`) will release the story for testing.

There's also a "Release" option in the `...` menu of the toolbar; all the commands mentioned are also available in the command palette and by right-clicking on an Inform 7 file in the explorer.

Those commands respect the Inform 7 project's settings (story format, binding into a Blorb file, making the random outcomes predictable, Inform version in macOS). You still need to edit these settings in the official Inform IDE for the moment, though.

The extension uses the default location of Inform for finding the compilers. (On Linux, it uses the installation of the command line interface, not the IDE. On macOS, it is only compatible with the interim 2019 release.)

You can override the compilers paths in the settings. (On Windows, this is particularly useful for the external path if your Documents folder is not located in its default location, for example on a disk other that `C:`; see below.)

## Extension settings

All the settings are prefixed with `inform7.` (e.g. `inform7.openStoryAfterCompilation`)

- `openStoryAfterCompilation`: If set to `"external"`, story files will be opened with their default application (e.g. Lectrote for `.ulx`) after being compiled. If set to `"vscode"`, story files will be opened in a VS Code tab (the [IF Player extension](https://marketplace.visualstudio.com/items?itemName=natrium729.if-player) is recommended).
- `openStoryColumn`: If `openStoryAfterCompilation` is set to `"vscode"`, the compiled story will be opened in the specified editor column.
- `paths.ni`: The path of ni used when compiling a story. If unset, Inform's default installation will be used.
- `paths.inform6`: The path of inform6 used when compiling a story. If unset, Inform's default installation will be used.
- `paths.cblorb`: The path of cBlorb used when releasing a story. If unset, Inform's default installation will be used.
- `paths.internal`: The path of the internal folder used when compiling a story. If unset, Inform's default installation will be used.
- `paths.external`: The path of the external folder used when compiling a story. If unset, Inform's default installation will be used. On Windows, this folder is located in Documents, but the extension cannot detect if Documents has bee moved to another location, e.g. not on `C:`; if that's the case, you'll have to set this setting.

## Missing features

The extension does not implement every features of the official Inform IDE. Ideally, they'll eventually be added to the extension. Meanwhile, you'll have to use the Inform IDE to use them.

Among missing features, in no particular order :

- Creating a project or an extension.
- Installing extensions (from files or the Public library).
- The index.
- The documentation.
- Jumping to a heading.
- Editing the project settings
- The skein/transcript/testing tabs.
- Cleaning build files and index when closing project.
- Generating Inform 6 debugging information.
- Elastic tabs between table columns.

## Known issues

- When modifying the settings of a project in the Inform 7 IDE while the project is open in VS Code, you'll need to restart VS Code for the changes to take effect.
