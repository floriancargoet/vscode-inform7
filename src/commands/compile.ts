import path = require("path")

import * as vscode from "vscode"

import { Command } from "../command-manager"
import { getInformProjectPathFor } from "../inform-project"
import { NiTaskProvider } from "../task-providers/ni-task-provider"
import { TaskManager } from "../task-manager"


/**
 * Command that execute the Inform 7 task to compile the file at the given Uri.
 *
 * If no Uri is given, the Uri of the active text editor is used.
 *
 * @param uri The uri of the file to compile.
 */
export class CompileCommand implements Command {
	readonly id = "inform7.compile"
	private taskManager: TaskManager

	constructor(taskManager: TaskManager) {
		this.taskManager = taskManager
	}

	async execute(uri: vscode.Uri | undefined) {
		// When using the command palette, no uri is provided.
		// We use the currently active editor in that case.
		if (!uri) {
			if (!vscode.window.activeTextEditor) {
				vscode.window.showErrorMessage("There is no open file to compile with Inform 7")
				return
			}
			uri = vscode.window.activeTextEditor.document.uri
		}

		// Get the path of the .inform project containing the .ni source
		const projectPath = await getInformProjectPathFor(uri)
		if (!projectPath) {
			vscode.window.showErrorMessage(`No project is associated with ${path.basename(uri.fsPath)}.`)
			return
		}

		vscode.tasks.executeTask(await this.taskManager.getNiTask({
			type: NiTaskProvider.NiTaskType,
			project: projectPath,
			mode: "test",
			toInform6: true
		}))
	}
}
