import os = require("os")
import path = require("path")

import * as vscode from "vscode"


export function getNiPath(version?: string) {
	const customNiPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("ni", "")
	if (customNiPath) {
		return customNiPath
	}
	switch (process.platform) {
		case "darwin":
			return version ?
				`/Applications/Inform.app/Contents/MacOS/${version}/ni`
				: "/Applications/Inform.app/Contents/MacOS/ni"
		case "linux":
			return "/usr/local/libexec/ni"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\ni.exe"
		default:
			return "ni"
	}
}


export function getInform6Path() {
	const customInform6Path = vscode.workspace.getConfiguration("inform7.paths").get<string>("inform6", "")
	if (customInform6Path) {
		return customInform6Path
	}
	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/inform6"
		case "linux":
			return "/usr/local/libexec/inform6"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\inform6.exe"
		default:
			return "inform6"
	}
}


export function getCblorbPath() {
	const customCblorbPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("cblorb", "")
	if (customCblorbPath) {
		return customCblorbPath
	}
	switch (process.platform) {
		case "darwin":
			return "/Applications/Inform.app/Contents/MacOS/cBlorb"
		case "linux":
			return "/usr/local/libexec/cBlorb"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Compilers\\cBlorb.exe"
		default:
			return "cBlorb"
	}
}


export function getInternalPath(version?: string) {
	const customInternalPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("internal", "")
	if (customInternalPath) {
		return customInternalPath
	}
	switch (process.platform) {
		case "darwin":
			return `/Applications/Inform.app/Contents/Resources/retrospective/${version || "6M62"}`
		case "linux":
			return "/usr/local/share/inform7/Internal"
		case "win32":
			return "C:\\Program Files (x86)\\Inform 7\\Internal"
		default:
			return ""
	}
}


export function getExternalPath() {
	const customExternalPath = vscode.workspace.getConfiguration("inform7.paths").get<string>("external", "")
	if (customExternalPath) {
		return customExternalPath
	}
	switch(process.platform) {
		case "darwin":
			return path.join(os.homedir(), "/Library/Inform")
		case "linux":
			return path.join(os.homedir(), "Inform")
		case "win32":
			return path.join(os.userInfo().homedir, "Documents\\Inform")
		default:
			return path.join(os.homedir(), "Inform")
	}
}
