import * as vscode from "vscode"


interface ManualProgressEventData {
	increment: number,
	message: string,
	complete?: boolean
}


/**
 * A helper class to show VS Code progresses that are updated manually.
 */
export class ManualProgress {
	private readonly disposables: vscode.Disposable[] = []
	private eventEmitter: vscode.EventEmitter<ManualProgressEventData>
	started = false

	constructor() {
		this.eventEmitter = new vscode.EventEmitter<ManualProgressEventData>()
		this.disposables.push(this.eventEmitter)
	}

	/**
	 * Start the progress and show it. Does nothing if already started.
	 */
	start(options: vscode.ProgressOptions) {
		if (this.started) {
			return
		}
		this.started = true

		vscode.window.withProgress(options, (progress, token) => {
			return new Promise(resolve => {
				this.eventEmitter.event(val => {
					progress.report(val)
					if (val.complete) {
						resolve()
					}
				}, this, this.disposables)
			})
		})
	}

	/**
	 * Update the progress. Does nothing if `start` hasn't been called yet.
	 */
	update(data: ManualProgressEventData) {
		if (!this.started) {
			return
		}
		this.eventEmitter.fire(data)
	}

	dispose() {
		this.update({ message: "", increment: 0, complete: true })
		for (const disposable of this.disposables) {
			disposable.dispose()
		}
	}
}
