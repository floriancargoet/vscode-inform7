import * as vscode from "vscode"

import { TaskManager } from "../task-manager"


export interface CblorbTaskDefinition extends vscode.TaskDefinition {
	/**
	 * The type of a cBlorb task is always `"cblorb"`
	 */
	type: "cblorb",

	/**
	 * The path to the `.inform` project to bind up into a Blorb file.
	 */
	project: string,

	/**
	 * The story format in which the Inform project has been compiled.
	 */
	format?: "z5" | "z8" | "ulx",
}


/* This function has to be updated when the definition above is changed!!! */
export function isCblorbTaskDefinition(definition: any): definition is CblorbTaskDefinition {
	// Check if it's a cBlorb task.
	if (definition.type !== CblorbTaskProvider.CblorbTaskType) {
		return false
	}

	// Check if the mandatory project is present.
	if (typeof definition.project !== "string") {
		return false
	}

	// Check the format.
	const format = definition.format ?? "ulx"
	if (format !== "z5" && format !== "z8" && format !== "ulx") {
		return false
	}

	return true
}


export class CblorbTaskProvider implements vscode.TaskProvider {
	static readonly CblorbTaskType = "cblorb"
	private taskManager: TaskManager

	constructor(taskManager: TaskManager) {
		this.taskManager = taskManager
	}

	async provideTasks(): Promise<vscode.Task[]> {
		// No tasks are auto-detected.
		return []
	}

	async resolveTask(_task: vscode.Task): Promise<vscode.Task | undefined> {
		if (isCblorbTaskDefinition(_task.definition)) {
			return await this.taskManager.getCblorbTask(_task.definition)
		}
		return undefined
	}
}
