/**
 * The Inform 6 for Inform 7 Task provider. Used only to compile Inform 6 source
 * resulting from ni.
 */

import path = require("path")

import * as vscode from "vscode"

import { TaskManager } from "../task-manager"


export interface Inform6nTaskDefinition extends vscode.TaskDefinition {
	/**
	 * The type of an Inform 6N task is always `inform6n`.
	 */
	type: "inform6n",

	/**
	 * The path of the Inform 7 project containing the Inform 6 source to compile.
	 */
	project: string,

	/**
	 * The story format in which the Inform project will be compiled.
	 */
	format?: "z5" | "z8" | "ulx",

	/**
	 * Whether or not to compile in debug mode.
	 */
	debug?: boolean,

	/**
	 * Whether or not to bind up the story into a Blorb file upon release.
	 */
	toCblorb?: boolean
}


/* This function has to be updated when the definition above is changed!!! */
export function isInform6nTaskDefinition(definition: any): definition is Inform6nTaskDefinition {
	// Check if it's a Inform 6n task.
	if (definition.type !== Inform6nTaskProvider.Inform6nTaskType) {
		return false
	}

	// Check if the mandatory project is present.
	if (typeof definition.project !== "string") {
		return false
	}

	// Check if format is present.
	const format = definition.format ?? "ulx"
	if (format !== "z5" && format !== "z8" && format !== "ulx") {
		return false
	}

	// Check debug.
	if (typeof (definition.debug ?? true) !== "boolean") {
		return false
	}

	// Check toCblorb.
	if (typeof (definition.toCblorb ?? false) !== "boolean") {
		return false
	}

	return true
}


export class Inform6nTaskProvider implements vscode.TaskProvider {
	static readonly Inform6nTaskType = "inform6n"
	private taskManager: TaskManager

	constructor(taskManager: TaskManager) {
		this.taskManager = taskManager
	}

	async provideTasks(): Promise<vscode.Task[]> {
		// No tasks are auto-detected since Inform 6N tasks are only used after Ni ones.
		return []
	}

	async resolveTask(_task: vscode.Task): Promise<vscode.Task | undefined> {
		if (isInform6nTaskDefinition(_task.definition)) {
			return await this.taskManager.getInform6nTask(_task.definition)
		}
		return undefined
	}
}
