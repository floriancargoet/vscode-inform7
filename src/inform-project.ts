import fsCallbacks = require("fs")
const fs = fsCallbacks.promises
import path = require("path")

import * as vscode from "vscode"
import plist = require("plist")


/**
 * Manages Inform-project-related things, such as project settings.
 */
export class InformProjectManager {
	// TODO: The settings are not updated in the cache when Settings.plist is edited.
	/**
	 * A project's settings will be cached here the first time they are read from disk.
	 */
	private readonly cachedSettings = new Map<string, any>()

	/**
	 * Get the settings associated with the given project.
	 * @param filePath - The Uri associated with an Inform project or a string representing its path.
	 */
	async getProjectSettings(filePath: vscode.Uri | string) {
		if (filePath instanceof vscode.Uri) {
			filePath = filePath.fsPath
		}

		// Try to retrieve the settings from cache first.
		if (this.cachedSettings.has(filePath)) {
			return this.cachedSettings.get(filePath)
		}

		// Read the settings plist from disk.
		let settings: string
		try {
			settings = await fs.readFile(path.join(filePath, "Settings.plist"), "utf8")
		} catch(err) {
			if (err.code === "ENOENT") {
				// TODO: Generate a new settings file.
				vscode.window.showErrorMessage("The project's settings file doesn't exist. Default values will be used")
				return null
			}
			vscode.window.showErrorMessage("The project's settings cannot be read. Default values will be used")
			return null
		}

		// Parse the plist into an object.
		let parsedSettings: any
		try {
			parsedSettings = plist.parse(settings)
		} catch {
			// TODO: Regenerate file.
			vscode.window.showErrorMessage("The project's settings file is malformed. Default values will be used")
			console.log("Cannot parse settings.")
			return null
		}
		this.cachedSettings.set(filePath, parsedSettings)
		return parsedSettings
	}

	/**
	 * Get the compiler version of the given project's settings. Returns a default value if it's not present.
	 * @param filePath - The Uri associated with an Inform project or a string representing its path.
	 */
	async getOutputSettingCompilerVersion(filePath: vscode.Uri | string) {
		const settings = await this.getProjectSettings(filePath)

		let version: string = settings?.IFOutputSettings?.IFSettingCompilerVersion
		if (typeof version !== "string") {
			version = defaultProjectSettings.IFOutputSettings.IFSettingCompilerVersion
		}
		return version
	}

	/**
	 * Get the story format of the given project's settings. Returns a default value if it's not present.
	 * @param filePath - The Uri associated with an Inform project or a string representing its path.
	 */
	async getOutputSettingStoryFormat(filePath: vscode.Uri | string) {
		const settings = await this.getProjectSettings(filePath)

		let formatNumber: number = settings?.IFOutputSettings?.IFSettingZCodeVersion
		if (typeof formatNumber !== "number") {
			formatNumber = defaultProjectSettings.IFOutputSettings.IFSettingZCodeVersion
		}

		switch (formatNumber) {
			case 5:
				return "z5"
			case 8:
				return "z8"
			/* Glulx is usually represented by 256 in the settings, but we accept any other value just in case. */
			default:
				return "ulx"
		}
	}

	/**
	 * Get the nobble RNG value of the given project's settings. Returns a default value if it's not present.
	 * @param filePath - The Uri associated with an Inform project or a string representing its path.
	 */
	async getOutputSettingNoRng(filePath: vscode.Uri | string) {
		const settings = await this.getProjectSettings(filePath)

		let noRng: boolean = settings?.IFOutputSettings?.IFSettingNobbleRng
		if (typeof noRng !== "boolean") {
			noRng = defaultProjectSettings.IFOutputSettings.IFSettingNobbleRng
		}
		return noRng
	}

	/**
	 * Get the create blorb value of the given project's settings. Returns a default value if it's not present.
	 * @param filePath - The Uri associated with an Inform project or a string representing its path.
	 */
	async getOutputSettingCreateBlorb(filePath: vscode.Uri | string) {
		const settings = await this.getProjectSettings(filePath)

		let blorb: boolean = settings?.IFOutputSettings?.IFSettingCreateBlorb
		if (typeof blorb !== "boolean") {
			blorb = defaultProjectSettings.IFOutputSettings.IFSettingCreateBlorb
		}
		return blorb
	}

	dispose() {
		this.cachedSettings.clear()
	}
}

/**
 * Object containing the default project settings. Used if a Settings.plist is
 * malformed.
 */
const defaultProjectSettings = {
	IFCompilerOptions: {
		IFSettingNaturalInform: true
	},
	IFInform6Extensions: {},
	IFLibrarySettings: {
		IFSettingLibraryToUse: "Natural"
	},
	IFMiscSettings : {
		IFSettingInfix: false
	},
	IFOutputSettings: {
		IFSettingCreateBlorb: true,
		IFSettingCompilerVersion: "6M62", // Only used on macOS at the moment.
		IFSettingNobbleRng: false,
		IFSettingZCodeVersion: 256, // Glulx.
	},
	IFRandomSettings: {}
}

/**
 * Get the path of the Inform project associated from the given path.
 *
 * - If the given path is an Inform project, return it.
 * - If it's a `.ni` file, return the project it's in.
 * - If it'S an Inform extension, return the project it's in, if it exists.
 * - Otherwise return an empty string.
 * @param filepath
 */
export async function getInformProjectPathFor(filepath: string | vscode.Uri): Promise<string> {
	if (filepath instanceof vscode.Uri) {
		filepath = filepath.fsPath
	}

	const parsedPath = path.parse(filepath)

	// It's already an Inform project.
	if (parsedPath.ext === ".inform") {
		return filepath
	// It's a .ni file.
	} else if (parsedPath.ext === ".ni") {
		filepath = path.dirname(path.dirname(filepath))
	// It's an extension, check if it's a project-specific one.
	} else if (parsedPath.ext === ".i7x") {
		const materialsPath = path.dirname(path.dirname(path.dirname(filepath)))
		const projectName = path.parse(materialsPath).name + ".inform"
		filepath = path.join(path.dirname(materialsPath), projectName)
	}

	// Check if the Inform project exists.
	let isDirectory: boolean
	try {
		const type = (await vscode.workspace.fs.stat(vscode.Uri.file(filepath))).type
		isDirectory = type === vscode.FileType.Directory
	} catch {
		return ""
	}
	if (isDirectory) {
		return filepath
	}

	return ""
}
