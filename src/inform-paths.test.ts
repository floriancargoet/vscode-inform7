import { getNiPath } from "./inform-paths"

// Mocking properties of a global is complicated… jest.mock() doesn't work.
// We need to copy/restore the whole property definition, not just the raw value.
const platformDescriptor = Object.getOwnPropertyDescriptor(process, "platform")!

type Platform = typeof process.platform
function mockPlatform(platform: Platform) {
	Object.defineProperty(process, "platform", {
		...platformDescriptor,
		value: platform,
	})
}
function restorePlatform() {
	Object.defineProperty(process, "platform", platformDescriptor)
}

describe("getNiPath()", () => {
	afterEach(() => {
		restorePlatform()
	})

	test("it returns the correct version on OSX", () => {
		// Pretend to be on OSX
		mockPlatform("darwin")

		expect(getNiPath()).toBe("/Applications/Inform.app/Contents/MacOS/ni")
		expect(getNiPath("6L38")).toBe("/Applications/Inform.app/Contents/MacOS/6L38/ni")
	})
})
