/**
 * Exposes the Visual Studio Code extension API to the Jest testing environment.
 *
 * Tests would otherwise not have access to it because they are sandboxed.
 *
 * @see jest-vscode-framework-setup.ts
 */

import * as vscode from 'vscode'
import NodeEnvironment = require('jest-environment-node')

class VsCodeEnvironment extends NodeEnvironment {
	public async setup() {
		await super.setup()
		this.global.vscode = vscode
	}

	public async teardown() {
		this.global.vscode = {}
		return await super.teardown()
	}
}

module.exports = VsCodeEnvironment
